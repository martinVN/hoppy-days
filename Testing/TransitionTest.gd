extends Node

func fade_in():
	$TransitionsLayer.show()
	$AnimationPlayer.play("FadeIn")

func fade_out():
		$TransitionsLayer.show()
		$AnimationPlayer.play("FadeOut")


func _on_smooth_pressed():
	var cur_value = $TransitionsLayer.material.get_shader_param("smooth_transition")
	$TransitionsLayer.material.set_shader_param("smooth_transition", !cur_value)
