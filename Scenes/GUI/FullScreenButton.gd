extends TextureButton

func _ready():
	if OS.get_name()=="HTML5":
		show()
	else:
		hide()


func _on_FullScreenButton_pressed():
	OS.window_maximized = !OS.window_maximized
