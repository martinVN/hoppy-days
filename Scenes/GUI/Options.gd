extends Control

onready var back = $VBoxContainer/TitleScreenButton
onready var master_volume = $VBoxContainer/Master
onready var music_volume = $VBoxContainer/Music
onready var sfx_volume = $VBoxContainer/SFX


func _on_back_pressed():
	var audio_settings = {
		"master": master_volume.get_value(),
		"music": music_volume.get_value(),
		"sfx": sfx_volume.get_value()
	}
	Global.ConfigManager.set_section("audio", audio_settings)
	Global.ConfigManager.save_settings()

	Global.goto_scene($VBoxContainer/TitleScreenButton.scene_to_load)
