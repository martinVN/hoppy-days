tool
extends Control

signal volume_changed(audio_bus, percent_value)

export (String) var audio_bus
export (String) var audio_bus_label setget set_label


func _ready():
	$Label.text = audio_bus_label
	var volume_decibel = _get_volume()
	var volume_percent = decibel_to_percent(volume_decibel)
	$HSlider.value = volume_percent


func get_value():
	return $HSlider.value


func _get_config_volume():
	if Engine.editor_hint:
		return 100.0
	var config_value = get_node("/root/Global").ConfigManager.get_setting("audio", audio_bus.to_lower())
	if not config_value:
		config_value = _get_volume()
	return int(config_value)

func _get_bus_id():
	return AudioServer.get_bus_index(audio_bus)

func set_label(_label):
	audio_bus_label = _label
	if has_node("Label"):
		$Label.text = audio_bus_label

func percent_to_decibel(_percent):
	return 20 * log(_percent / 100.0)/log(10)

func decibel_to_percent(_decibel):
	return round(100 * pow(10, _decibel/20))


func _set_volume(volume_percent):
	if volume_percent == 0:
		AudioServer.set_bus_mute(_get_bus_id(), true)
	else:
		AudioServer.set_bus_mute(_get_bus_id(), false)
		var decibel = percent_to_decibel(volume_percent)
		AudioServer.set_bus_volume_db(_get_bus_id(), decibel)
	emit_signal("volume_changed", audio_bus, volume_percent)


func _get_volume():
	return AudioServer.get_bus_volume_db(_get_bus_id())

func _on_HSlider_value_changed(volume_precent):
	if volume_precent > 0:
		$Value.text = "%s%% " % volume_precent
	else:
		$Value.text = "OFF"

	_set_volume(volume_precent)
