tool
extends Button

export (String) var label setget set_label
export (String) var scene_to_load


func set_label(_label):
	label = _label
	if has_node("Label"):
		$Label.text = label