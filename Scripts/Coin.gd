extends Area2D

func _ready():
	$AnimatedSprite.playing = true
	$AnimationPlayer.play("Wobble")


func _on_Coin_body_entered(body):
	# Since the object is not instantly removed,
	# the player could walk out and in again before
	# it is removed. Let's just disable it to avoid this.
	# Function blocked during in/out signal. Use call_deferred("set_monitoring",true/false)
	call_deferred("set_monitoring",false)
	Global.Level.coin_pickup()
	$AnimationPlayer.play("Pickup")
	play_sound()


func play_sound():
	var current_coin_count = Global.GameState.coins
	var pitch = 0.9 + (current_coin_count / 21.0) * 0.2
	$CoinSond.pitch_scale = pitch
	$CoinSond.play()

func faster_animation():
	var prev_anim_frame = $AnimatedSprite.frame
	$AnimatedSprite.play("fast")
	$AnimatedSprite.frame = prev_anim_frame