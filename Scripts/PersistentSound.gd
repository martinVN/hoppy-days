extends Node

var audio_busses = ["Master", "Music", "SFX"]

func _ready():
	for bus in audio_busses:
		var volume = Global.ConfigManager.get_setting("audio", bus.to_lower())
		if volume:
			set_bus_volume(bus, volume)
		else:
			print("No volume setting for bus: %s" %bus)


func percent_to_decibel(_percent):
	return 20 * log(_percent / 100.0)/log(10)

func set_bus_volume(bus, volume_percent):
	var bus_id = AudioServer.get_bus_index(bus)
	if bus_id != -1:
		AudioServer.set_bus_volume_db(bus_id, percent_to_decibel(volume_percent))
	else:
		print("trying to set volume on an unknown bus: %s" % bus)
