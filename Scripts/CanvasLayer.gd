extends CanvasLayer

func start():
	$Control.show()
	$Control/AnimationPlayer.play("FadeOut")
	$Control/LevelLabel.text = "Level %s" %Global.GameState.current_level

func _on_AnimationPlayer_animation_finished(anim_name):
	pass
