extends KinematicBody2D

const SPEED = 750
const GRAVITY = 3600
const JUMP_SPEED = -1500
const KNOCKBACK_FORCE = 2000
const UP = Vector2(0, -1)

var motion = Vector2()
var knockback_motion = Vector2()
onready var camera = find_node("Camera2D")
var can_jump = true

func _ready():
	show()
	Global.Player = self

func _process(delta):
	update_animation(motion)


func update_animation(motion):
	$AnimatedSprite.update(motion)


func _physics_process(delta):
	update_movement(delta)
	motion = move_and_slide(motion + knockback_motion, UP)
	motion -= knockback_motion


func update_movement(delta):
	run()
	fall(delta)
	knockback()
	jump()


func fall(delta):
	print(is_on_floor())
#    if is_on_floor():
#        motion.y = 10
#    else:
#        motion.y += GRAVITY * delta
	motion.y += GRAVITY * delta

	if camera and position.y > camera.limit_bottom:
		print("Fell out of world")
		Global.Level.fell_out_of_world()


func jump():
	if Input.is_action_pressed("ui_up") and is_on_floor() and can_jump:
		motion.y = JUMP_SPEED
		can_jump = false
	if Input.is_action_just_released("ui_up"):
		can_jump = true
		if motion.y < 0:
			motion.y *= 0.25


func knockback():
	if knockback_motion.length() == 0:
		return
	knockback_motion *= 0.9
	if knockback_motion.length() < 100:
		knockback_motion = Vector2()

func on_knockback(source_pos):
	motion = Vector2()
	var knockback_dir = (position - source_pos).normalized()
	knockback_velocity = knockback_dir * KNOCKBACK_FORCE


func run():
	motion.x = 0
	if Input.is_action_pressed("ui_right"):
		motion.x += SPEED
	if Input.is_action_pressed("ui_left"):
		motion.x += -SPEED
