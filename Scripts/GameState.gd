extends Node2D

signal coins_changed(_coins)
signal lives_changed(_lives)

var lives = 5 setget set_lives
var coins = 0 setget set_coins
var current_level = 0 setget set_level

func _ready():
	add_to_group("Persist")
	Global.GameState = self

func set_lives(_lives):
	lives = _lives
	emit_signal("lives_changed", lives)


func set_coins(_coins):
	coins = _coins
	emit_signal("coins_changed", coins)


func set_level(_level):
	current_level = _level


func save():
	var save_dict  = {
			"filename" : get_filename(),
			"parent" : get_parent().get_path(),
			"name" : get_name(),
			"pos_x" : position.x, # Vector2 is not supported by JSON
			"pos_y" : position.y,
			"lives": lives,
			"coins": coins,
			"current_level": current_level
	}
	return save_dict