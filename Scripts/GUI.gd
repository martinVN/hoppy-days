extends CanvasLayer

onready var lives_label = find_node("Lives").find_node("Label")
onready var coins_label = find_node("Coins").find_node("Label")
onready var lives_anim = find_node("Lives").find_node("AnimationPlayer")
onready var coins_anim = find_node("Coins").find_node("AnimationPlayer")

func _ready():
	$TopGUI.show()
	Global.GUI = self
	update_lives(Global.GameState.lives)
	update_coins(Global.GameState.coins)

func update_lives(_lives):
	lives_label.text = str(_lives)
	lives_anim.play("pulse_green")
	lives_anim.play("pulse")



func update_coins(_coins):
	coins_label.text = str(_coins)
	coins_anim.play("pulse")
