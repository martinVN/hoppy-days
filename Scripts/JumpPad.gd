extends Area2D


func _ready():
	$AnimatedSprite.play("idle")

func _on_JumpPad_body_entered(body):
	body.apply_jump(2.0)
	$AnimatedSprite.play("boost")
	$Timer.start()


func _on_Timer_timeout():
	$AnimatedSprite.play("idle")
