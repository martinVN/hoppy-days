extends Node

#Gloabal variables
var Level
var Player
var GUI

#Managers
var ConfigManager = preload("res://config_file.gd").new()
var SceneManager = preload("res://Scenes/Managers/SceneManager.tscn").instance()
var GameState = preload("res://Scenes/Managers/GameState.tscn").instance()
var PersistentSound = preload("res://Scenes/PersistentSound.tscn").instance()

#Helper variables for scene_loading
var scenes_for_loading = {
	"game_over": "res://Scenes/GameOver.tscn",
	"victory":  "res://Scenes/Victory.tscn",
	"title_screen" : "res://Scenes/TitleScreen.tscn",
	"options": "res://Scenes/GUI/Options.tscn"
	}

const LEVEL_FOLDER = "res://Scenes/Levels"
const path_to_save_file = "user://savegame.save"
enum COLLISION_LAYERS  {PLAYER = 0, ENVIRONMENT = 1, ENEMIES = 2, PICKUPS = 3}

var levels


func _ready():
	add_child(SceneManager)
	add_child(GameState)
	add_child(PersistentSound)
	levels = find_levels(LEVEL_FOLDER)

func find_levels(path):
	print("Scanning level directory for levels")
	var dir = Directory.new()
	var levels_in_dir = []
	if dir.open(path) == OK:
		dir.list_dir_begin(true)
		var file_name = dir.get_next()
		while (file_name != ""):
			if not dir.current_is_dir() and is_level_file(file_name):
				print("Found file: " + file_name)
				levels_in_dir.append(path + "/" + file_name)
			file_name = dir.get_next()
	else:
		print("An error occurred when trying to access the level folder.")
	print("Found %s levels" % levels_in_dir.size())
	print("Sorting levels found")

	levels_in_dir.sort_custom(LevelSorter, "sort")
	return levels_in_dir

func is_level_file(filename):
	return filename.match("Level*.tscn")
#	if not file_name.begins_with("Level"):
#		return false
#	if filename.get_extension() != "tscn":
#		return false
#	return true

func goto_scene(scene_name):
	SceneManager._goto_scene(scenes_for_loading[scene_name])


func goto_level(level):
	if level > levels.size():
		goto_scene("victory")
	else:
		GameState.current_level = level
		SceneManager._goto_scene(levels[level-1])



func save_game():
	var save_game = File.new()
	save_game.open(path_to_save_file, File.WRITE)
	var save_nodes = get_tree().get_nodes_in_group("Persist")
	for i in save_nodes:
		var node_data = i.call("save");
		save_game.store_line(to_json(node_data))
	save_game.close()


func load_game():
	var save_game = File.new()
	if not save_game.file_exists(path_to_save_file):
		return # Error! We don't have a save to load.

	# We need to revert the game state so we're not cloning objects during loading. This will vary wildly depending on the needs of a project, so take care with this step.
	# For our example, we will accomplish this by deleting savable objects.
	var save_nodes = get_tree().get_nodes_in_group("Persist")
	for i in save_nodes:
		i.queue_free()

	# Load the file line by line and process that dictionary to restore the object it represents
	save_game.open(path_to_save_file, File.READ)
	while not save_game.eof_reached():
		var current_line = parse_json(save_game.get_line())
		if not current_line:
			break
		print(current_line)
		# First we need to create the object and add it to the tree and set its position.
		var new_object = load(current_line["filename"]).instance()
		get_node(current_line["parent"]).add_child(new_object)
		new_object.position = Vector2(current_line["pos_x"], current_line["pos_y"])
		new_object.name = current_line["name"]
		# Now we set the remaining variables.
		for i in current_line.keys():
			if i == "filename" or i == "parent" or i == "pos_x" or i == "pos_y":
				continue
			new_object.set(i, current_line[i])
	save_game.close()
	goto_level(GameState.current_level)


class LevelSorter:
	static func sort(a,b):
		var a_level = int(a.get_file().get_basename().split("Level")[-1])
		var b_level = int(b.get_file().get_basename().split("Level")[-1])
		return a_level < b_level

