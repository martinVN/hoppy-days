extends Area2D

const SPEED = 200

var xpos = 0

func _ready():
	xpos = global_position.x

func _process(delta):
	move_down_screen(delta)


func move_down_screen(delta):
	global_position.y += SPEED * delta
	global_position.x = xpos

func _on_VisibilityNotifier2D_screen_exited():
	queue_free()


func _on_Lightning_body_entered(body):
	if body.get_collision_layer_bit(Global.COLLISION_LAYERS.PLAYER):
		Global.Level.take_damage()

	queue_free()
