extends Node2D

onready var hurt = $Hurt
onready var life_gain = $LifeGain

func _on_took_damage():
	hurt.pitch_scale = rand_range(0.9, 1.1)
	hurt.play()

func _on_life_gain():
#	life_gain.pitch_scale = rand_range(0.9, 1.1)
	life_gain.play()
