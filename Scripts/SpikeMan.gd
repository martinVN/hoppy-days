tool
extends Node2D


export (int) var left_limit = 100 setget set_left_limit
export (int) var right_limit = 100 setget set_right_limit
export (int) var walk_speed = 100
export (float) var idle_time = 1.0

var turn_limit = 10

onready var initial_pos

enum STATE{
	IDLE,
	WALKING_LEFT,
	WALKING_RIGHT
}

var state = STATE.IDLE
var prev_state
var idle_time_count = rand_range(0, 2)

func set_left_limit(newVal):
	left_limit = newVal
	update()

func set_right_limit(newVal):
	right_limit = newVal
	update()

func _ready():
	initial_pos = position.x


func _process(delta):
	if Engine.editor_hint:
		return
	update_state()
	var motion = move(delta)
	updateAnimation(motion)

func update_state():
	if state == STATE.WALKING_LEFT and position.x - (initial_pos - left_limit)   < turn_limit:
		prev_state = state
		state = STATE.IDLE
		idle_time_count = 0
	if state == STATE.WALKING_RIGHT and initial_pos + right_limit - position.x < turn_limit:
		prev_state = state
		state = STATE.IDLE
		idle_time_count = 0
	if state == STATE.IDLE and idle_time_count >= idle_time:
		state = STATE.WALKING_LEFT if prev_state == STATE.WALKING_RIGHT else STATE.WALKING_RIGHT


func move(delta):
	var motion = 0
	if state == STATE.IDLE:
		idle_time_count += delta
	elif state == STATE.WALKING_LEFT:
		motion -= 1
	else:
		motion = 1

	position.x += motion * delta * walk_speed
	return motion

func updateAnimation(motion):
	var anim = get_node("Area2D/AnimatedSprite")
	if motion == 0:
		anim.play("idle")
	if motion < 0:
		anim.play("walk")
		anim.flip_h = true
	elif motion > 0:
		anim.play("walk")
		anim.flip_h = false


func _draw():
	if not Engine.editor_hint:
		return
	var y_offset = 50
	var red = Color(1, 0, 0)
	draw_line(Vector2(0, y_offset), Vector2(-left_limit, y_offset), red, 4.0)
	draw_circle(Vector2(-left_limit, y_offset), 8.0, red)
	draw_line(Vector2(0 ,y_offset), Vector2(right_limit, y_offset), red, 4.0)
	draw_circle( Vector2(right_limit, y_offset), 8.0, red)
