tool
extends "res://Scripts/SpikeMan.gd"

onready var ray_cast = $Sprite/RayCast2D
onready var shoot_timer = $Sprite/Timer
onready var sprite = $Sprite

export (PackedScene) var lightning_scene

var can_shoot = true

func updateAnimation(motion):
	if motion < 0:
		sprite.flip_h = false
	elif motion > 0:
		sprite.flip_h = true

func _process(delta):
	._process(delta) #Call base process for movement
	if can_shoot and ray_cast.is_colliding():
		fire()


func fire():
	var lightning_instance = lightning_scene.instance()
	ray_cast.add_child(lightning_instance)

	shoot_timer.start()
	can_shoot = false

func _on_Timer_timeout():
	can_shoot = true