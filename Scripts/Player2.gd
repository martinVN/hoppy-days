extends KinematicBody2D

const UP = Vector2(0,-1)

const GRAVITY = 2000
const FALLING_GRAVITY = GRAVITY*2
const WALK_FORCE = 3000
const JUMP_SPEED = 1200
const WALK_MAX_SPEED = 700
const FRICTION = 0.10
const SMALL_JUMP_FRICTION = 0.8
const MAX_GHOST_JUMP = 0.2
const MIN_JUMP_TIME = 0.1
const MAX_JUMPS = 2
const KNOCKBACK_FORCE = 750
const KNOCKBACK_TIMER = 0.2

var velocity = Vector2()
var knockback_v = Vector2()

var jumping = false
var ghost_jump_timer = 1000
var jump_timer = 1000
var input_block_timer = 0

var prev_jump_pressed = 0

onready var camera = find_node("Camera2D")

func _ready():
	show()
	Global.Player = self

func _process(delta):
	update_animation(velocity)
	is_out_of_level_check()


func update_animation(velocity):
	$AnimatedSprite.update(velocity)

func on_knockback(source_pos):
	velocity = Vector2()
	var knockback_dir = (position - source_pos).normalized()
	knockback_v = knockback_dir * KNOCKBACK_FORCE
	input_block_timer = KNOCKBACK_TIMER

func on_took_damage():
	$AnimationPlayer.play("Pulse_red")

func is_out_of_level_check():
	if camera and position.y > camera.limit_bottom:
		Global.Level.fell_out_of_world()

func is_input_blocked(delta):
	input_block_timer -= delta
	return input_block_timer > 0

func _physics_process(delta):
	var force
	if is_falling():
		force = Vector2(0,FALLING_GRAVITY)
	else:
		force = Vector2(0,GRAVITY)

	var walk_left = Input.is_action_pressed("ui_left")
	var walk_right = Input.is_action_pressed("ui_right")
	var jump = Input.is_action_just_pressed("ui_up")
	var stop_jump = not Input.is_action_pressed("ui_up")

	if not is_input_blocked(delta):
		if walk_left and velocity.x > -WALK_MAX_SPEED:
			force.x -= WALK_FORCE
		if walk_right and velocity.x < WALK_MAX_SPEED:
			force.x += WALK_FORCE
		if (walk_right and walk_left) or (not walk_right and not walk_left):
			velocity.x = lerp(velocity.x, 0, 1.0-FRICTION)
		if (stop_jump and jumping and jump_timer > MIN_JUMP_TIME):
			velocity.y = lerp(velocity.y, 0, 1.0-FRICTION)

	velocity += knockback_v
	knockback_v = Vector2()


	velocity += force * delta
	velocity = move_and_slide(velocity, UP)
	if is_on_floor():
		ghost_jump_timer = 0
		prev_jump_pressed = 0
	else:
		ghost_jump_timer += delta

	if jumping:
		jump_timer += delta

	if jumping and is_falling(): #Now faling
		jumping = false

	if jump and (can_jump() || can_double_jump()):
		prev_jump_pressed += 1
		apply_jump()
#		velocity.y = -JUMP_SPEED
		jumping = true
		jump_timer = 0



func can_jump():
	return ghost_jump_timer < MAX_GHOST_JUMP and prev_jump_pressed < MAX_JUMPS and not jumping


func apply_jump(multiplier = 1.0):
	velocity.y = -JUMP_SPEED * multiplier


func can_double_jump():
	if prev_jump_pressed == 0:
		return false
	if prev_jump_pressed >= MAX_JUMPS:
		return false
	return true

func is_falling():
	return velocity.y > 0

