extends Area2D

func _on_SpikesTop_body_entered(body):
	Global.Level.take_damage()
	body.on_knockback(global_position)
	if has_node("AnimationPlayer"):
		$AnimationPlayer.play("Die")
