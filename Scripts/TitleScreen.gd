extends Control

var scene_to_load

func _ready():
	$Menu/CenterRow/Buttons/ContinueButton.disabled = not File.new().file_exists(Global.path_to_save_file)

	for button in $Menu/CenterRow/Buttons.get_children():
		button.connect("pressed", self, "_on_button_pressed", [button.scene_to_load])


func _on_button_pressed(scene):
	scene_to_load = scene
	$FadeIn.show()
	$FadeIn.fade_in()

func _on_FadeIn_fade_finished():
	match scene_to_load:
		"new_game":
			Global.goto_level(1)
		"continue":
			Global.load_game()
		"options":
			Global.goto_scene(scene_to_load)
