extends Node2D

export var top_limit_margin = 800
export var limit_margin = 800

signal coin_pickup
signal took_damage
signal gained_life

var coins_for_new_life = 20

func _ready():
	Global.Level = self
	GUI_connect()
	set_camera_limits()
	place_player_at_start()
	if (Global.GameState.current_level == 1):
		init_game_state()
	$FadeOut.start()
	Global.save_game()


func init_game_state():
	var gameState = Global.GameState
	gameState.lives = 5
	gameState.coins = 0


func GUI_connect():
	Global.GameState.connect("lives_changed", $GUI, "update_lives")
	Global.GameState.connect("coins_changed", $GUI, "update_coins")


func set_camera_limits():
	var tilemap_rect = $Map.get_used_rect()
	var world_pos = $Map.map_to_world(tilemap_rect.position)
	var world_end = $Map.map_to_world(tilemap_rect.end)

	# Adjusting for Tilemap position in the world
	world_pos += $Map.position
	world_end += $Map.position

	#Set camera limits
	var Camera = Global.Player.find_node("Camera2D")
	Camera.limit_left = world_pos.x - limit_margin
	Camera.limit_top = world_pos.y - top_limit_margin
	Camera.limit_right = world_end.x + limit_margin
	Camera.limit_bottom = world_end.y + limit_margin


func end_game():
	Global.GameState.current_level = 0
	Global.goto_scene("game_over")



func place_player_at_start():
	Global.Player.position = $StartPosition.position

func fell_out_of_world():
	take_damage()
	place_player_at_start()



func take_damage():
	Global.GameState.lives -= 1
	if Global.GameState.lives <= 0:
		end_game()
	else:
		emit_signal("took_damage")


func coin_pickup():
	Global.GameState.coins += 1
	emit_signal("coin_pickup")
	var new_lives = floor(Global.GameState.coins / coins_for_new_life)
	if new_lives > 0:
		Global.GameState.coins -= new_lives * coins_for_new_life
		Global.GameState.lives += new_lives
		emit_signal("gained_life")
