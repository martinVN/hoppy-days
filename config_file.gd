const SAVE_PATH = "user://config.cfg"

var _config_file = ConfigFile.new()
var _settings = {
	"audio":{
		"master" : 50,
		"music"  : 100,
		"sfx" 	 : 100
	}
}

func _init():
	#check if there is a config file already, create a default one if not.
	var config_file = File.new()
	if not config_file.file_exists(SAVE_PATH):
		save_settings()
	load_settings()


func save_settings():
	for section in _settings.keys():
		for key in _settings[section].keys():
			_config_file.set_value(section, key, get_setting(section, key))
	print("saving file")
	_config_file.save(SAVE_PATH)

func load_settings():
	var error = _config_file.load(SAVE_PATH)
	if error != OK:
		print("Error loading the settings. Error code: %s" % error)
		return

	for section in _config_file.get_sections():
		print("%s:" % section)
		for key in _config_file.get_section_keys(section):
			var val = _config_file.get_value(section,key, get_setting(section,key))
			set_setting(section, key, val)
			# Printing the values for debug purposes
			print("\t%s: %s" % [key, val])


func get_setting(section, key):
	if _settings.has(section) and _settings[section].has(key):
		return _settings[section][key]
	else:
		return null


func set_setting(section, key, value):
	if not _settings.has(section):
		_settings[section] = {}
	_settings[section][key] = value

func set_section(section, value_dict):
	for key in value_dict.keys():
		set_setting(section, key, value_dict[key])