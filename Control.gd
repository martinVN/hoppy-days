tool
extends Control

var button_group


func _on_Button_pressed():
	$FileDialog.popup()


func _on_FileDialog_file_selected(path):
	var asset_reference = load("res://addons/my_custom_dock/Asset_icon.tscn").instance()
	asset_reference.set_asset(path)
	asset_reference.connect("button_pressed", self, "on_button_press")
	if button_group:
		asset_reference.set_button_group(button_group)
	else:
		button_group = asset_reference.get_button_group()
	$VBoxContainer/GridContainer.add_child(asset_reference)

func on_button_press():
	print(button_group.get_pressed_button())