tool
extends EditorPlugin

var dock # A class member to hold the dock during the plugin lifecycle
var current_screen_name

func _enter_tree():
	# Initialization of the plugin goes here
	# First load the dock scene and instance it:
	dock = preload("res://addons/my_custom_dock/my_dock.tscn").instance()

	# Add the loaded scene to the docks:
	add_control_to_dock(DOCK_SLOT_LEFT_UR, dock)
	connect("main_screen_changed", self, "screen_changed")

func _exit_tree():
	# Clean-up of the plugin goes here
	# Remove the scene from the docks:
	remove_control_from_docks(dock) # Remove the dock
	dock.free() # Erase the control from the memory

func _input(event):
	if current_screen_name != "2D":
		return

	if event is InputEventMouseButton and event.is_pressed() and event.button_index == BUTTON_LEFT:
		var _2d_viewport = get_2d_viewport()
		if (not _2d_viewport.get_global_rect().has_point(event.position)):
			return
		var root_node = get_tree().get_edited_scene_root()
		var viewport = root_node.get_viewport()
		var mousePos = viewport.get_mouse_position()
		print(mousePos)
		var asset_to_place = get_current_asset()
		if asset_to_place:
			place_instance(mousePos, asset_to_place)


func screen_changed(screen):
	current_screen_name = screen


func get_current_asset():
	if not dock.button_group:
		print("No button group")
		return
	var active_button = dock.button_group.get_pressed_button()
	if not active_button:
		print("no active button")
		return
	return active_button.get_asset_ref()

func get_2d_viewport():
	var editor_viewport = get_editor_interface().get_editor_viewport()
	return get_node_with_class(editor_viewport, "CanvasItemEditorViewport")


func get_node_with_class(start_node, target_class):
	var nodes = [start_node]
	while nodes.size() > 0:
		var cur_node = nodes.pop_front()
		if cur_node.get_class() == target_class:
			return cur_node
		else:
			for child in cur_node.get_children():
				nodes.append(child)

func place_instance(position, instance):
	var root = get_tree().get_edited_scene_root()
	var duplicate_instance = instance.duplicate()
	duplicate_instance.position = position
	root.add_child(duplicate_instance)
	duplicate_instance.set_owner(root)

