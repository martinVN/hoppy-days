tool
extends Control

var asset_path = ""
var instance

signal button_pressed

func set_asset(path):
	asset_path = path
	instance = load(path).instance()
	var asset_name = path.split("/")[-1]
	$VBoxContainer/Label.text = asset_name
	var sprite = instance.find_node("Sprite")
	var animated_sprite = instance.find_node("AnimatedSprite")
	if sprite:
		set_icon(sprite.texture)
	elif animated_sprite:
		print("found animated sprite")
		var sprite_frames = animated_sprite.frames
		var animation = animated_sprite.animation
		var tex = sprite_frames.get_frame(animation, 0)
		set_icon(tex)


func set_icon(sprite):
	$VBoxContainer/TextureButton.texture_normal = sprite


func _on_TextureButton_pressed():
	print("pressed: %s" %asset_path)


func _on_TextureButton_toggled(button_pressed):
	emit_signal("button_pressed")


func get_button_group():
	return $VBoxContainer/TextureButton.group

func set_button_group(group):
	$VBoxContainer/TextureButton.group = group
